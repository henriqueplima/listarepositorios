//
//  ErrorView.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 12/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit

protocol ErrorDelegate {
    
    func chamarController()
    
}

class ErrorView: UIView {

    @IBOutlet weak var btnTentar: UIButton!
    var delegate : ErrorDelegate?
    @IBAction func acaoTentar(_ sender: Any) {
        
        self.removeFromSuperview()
        self.delegate?.chamarController()
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
