//
//  RepoTableCell.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 11/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit

class RepoTableCell: UITableViewCell {

    @IBOutlet weak var lblRepoNome: UILabel!
    @IBOutlet weak var lblRepoDescricao: UILabel!
    @IBOutlet weak var lblNomeUser: UILabel!
    @IBOutlet weak var lblEstrela: UILabel!
    @IBOutlet weak var lblFork: UILabel!
    @IBOutlet weak var imgAutor: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Util.arredondarImageView(self.imgAutor)
    }
    
    func configCell (repo:Repositorio) {
        
        self.lblNomeUser.text = repo.autor
        self.lblRepoNome.text = repo.nome
        self.lblRepoDescricao.text = repo.descricao
        if let fork = repo.forks {
            self.lblFork.text = "\(fork)"
        }
        if let estrela = repo.estrelas {
            self.lblEstrela.text = "\(estrela)"
        }
//        self.lblFork.text = String(describing: repo.forks)
//        self.lblEstrela.text = String(describing: repo.estrelas)
        Service.downloadFotoUsuario(url: repo.foto!) { (dataImage) in
            DispatchQueue.main.async {
                let image = UIImage.init(data: dataImage)
                self.imgAutor.image = image
            }
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
