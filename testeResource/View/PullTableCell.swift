//
//  PullTableCell.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 12/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit

class PullTableCell: UITableViewCell {

    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblBody: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var lblData: UILabel!
    
    
    func configCell (pull:Pull) {
        
        self.lblTitulo.text = pull.titulo
        self.lblBody.text = pull.body
        self.lblUser.text = pull.nomeAutor
        self.lblData.text = pull.data
        Service.downloadFotoUsuario(url: pull.fotoAutor!) { (dataImage) in
            DispatchQueue.main.async {
                let image = UIImage.init(data: dataImage)
                self.imgUser.image = image
            }
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Util.arredondarImageView(self.imgUser)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
