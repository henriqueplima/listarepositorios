//
//  PullViewController.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 11/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit

class PullViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ErrorDelegate {

    @IBOutlet weak var tableView: UITableView!
    var texto:String?
    var gerenciador : GerenciadorRepo?
    let servico = Service()
    let PULL_CELL = "pullCell"
    let PULL_NIB = "PullTableCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.registerCells()
        self.chamarAPI()
        
        
    }
    
    func chamarAPI()  {
        
        guard ReachabilityManager.estaConectado() else {
            self.carregarTelaErro()
            return
        }
        
        self.gerenciador?.buscarPulls(carregar: {
            self.tableView.reloadData()
        }, falha: {
            self.carregarTelaErro()
        })
        
    }
    
    func carregarTelaErro() {
        
        let errorView : ErrorView = Bundle.main.loadNibNamed("ErrorVIew", owner: self, options: nil)?.first as! ErrorView
        errorView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        errorView.delegate = self
        
        self.view.addSubview(errorView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Error Delegate
    
    func chamarController() {
        
        chamarAPI()
        
    }
    
    func registerCells() {
        self.tableView.register(UINib.init(nibName: PULL_NIB, bundle: nil), forCellReuseIdentifier: PULL_CELL)
    }
    
    // MARK: TABLEW VIEW DATASOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.gerenciador?.pulls.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PullTableCell = tableView.dequeueReusableCell(withIdentifier: PULL_CELL) as! PullTableCell
        
        cell.configCell(pull: (self.gerenciador?.pulls[indexPath.row])!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard ReachabilityManager.estaConectado() else {
            self.carregarTelaErro()
            return
        }
        
            let url = URL.init(string: (self.gerenciador?.pulls[indexPath.row].urlPagina)!)
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
