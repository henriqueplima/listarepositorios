//
//  ViewController.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 10/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, ErrorDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    let gerenciador = GerenciadorRepo()
    let REPO_CELL = "repositorioCell"
    let REPO_NIB = "RepoTableCell"
    
    var servico : Service = Service()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerCells()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.adicioandoSpinner()
        self.chamarAPI()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func chamarController() {
        
        self.chamarAPI()
        
    }
    
    func carregarTelaErro() {
        
        let errorView : ErrorView = Bundle.main.loadNibNamed("ErrorVIew", owner: self, options: nil)?.first as! ErrorView
        errorView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        errorView.delegate = self
        self.view.addSubview(errorView)

    }
    
    func chamarAPI()  {
        
        guard ReachabilityManager.estaConectado() else {
            self.carregarTelaErro()
            return
        }
        
        gerenciador.buscarRepositorios(carregar: {
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }) {
            self.carregarTelaErro()
        }
    }
    
    // MARK: TABLEW VIEW DATASOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gerenciador.repositorios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RepoTableCell = tableView.dequeueReusableCell(withIdentifier: REPO_CELL) as! RepoTableCell
        
        cell.configCell(repo: self.gerenciador.repositorios[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == gerenciador.repositorios.count - 1) {
            chamarAPI()
        }
    }
    
    // MARK: TABLEW VIEW DELEGATE
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard ReachabilityManager.estaConectado() else {
            self.carregarTelaErro()
            return
        }
        
        gerenciador.repoSelecioando = gerenciador.repositorios[indexPath.row]
        self.performSegue(withIdentifier: "pull", sender: self)
        
    }
    
    func registerCells() {
        self.tableView.register(UINib.init(nibName: REPO_NIB, bundle: nil), forCellReuseIdentifier: REPO_CELL)
    }
    
    func adicioandoSpinner() {
        
        let spinner = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect.init(x: 0, y: 0, width: self.tableView.frame.width, height: 44)
        self.tableView.tableFooterView = spinner
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination as! PullViewController
        vc.gerenciador = self.gerenciador
        
    }


}

