//
//  Util.h
//  testeResource
//
//  Created by Henrique Pereira de Lima on 12/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Util : NSObject

+ (NSString *)transformaData:(NSString *)dataServidor;
+ (void)arredondarImageView:(UIImageView *)imageVIew;
@end
