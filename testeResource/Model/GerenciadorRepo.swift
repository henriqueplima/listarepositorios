//
//  GerenciadorRepo.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 11/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit

typealias CarregarTelaRepositorio = ()->Void
typealias FalhaCarregar = ()->Void

class GerenciadorRepo: NSObject {

    var repositorios : [Repositorio] = [];
    var servico : Service = Service()
    var repoSelecioando : Repositorio?
    var pulls : [Pull] = [];
    
    func buscarRepositorios(carregar:@escaping CarregarTelaRepositorio, falha:@escaping FalhaCarregar) -> Void {
        
        servico.buscarRepositorios(sucesso: { (arrayRepo) in
            self.repositorios = arrayRepo
            carregar()
        }, falha: {
            falha()
        }, arrayRepositorios: self.repositorios)
    }
    
    func buscarPulls(carregar:@escaping CarregarTelaRepositorio, falha:@escaping FalhaCarregar) -> Void {
        
        servico.buscarPulls(nomeRepo: (self.repoSelecioando?.nome)!, nomeUser: (self.repoSelecioando?.autor)!, sucesso: { (arrayPull) in
            self.pulls = arrayPull
            carregar()
        }) {
            falha()
        }
        
    }
    
}
