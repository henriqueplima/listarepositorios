//
//  Service.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 10/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit
import Alamofire

typealias SucessoRepositorio = ([Repositorio]) -> Void
typealias SucessoImage = (Data) -> Void
typealias SucessoPull = ([Pull]) -> Void

struct ConstJson {
    static let ITEMS = "items"
    static let REPO_NOME = "name"
    static let REPO_DESCRICAO = "description"
    static let AUTOR = "login"
    static let FOTO = "avatar_url"
    static let ESTRELAS = "stargazers_count"
    static let FORKS = "forks_count"
    static let USER = "owner"
    static let PULL_TITLE = "title"
    static let PULL_DATE = "updated_at"
    static let PULL_BODY = "body"
    static let PULL_USER = "user"
    static let PULL_ULR = "html_url"
}

class Service: NSObject {
    var paginaRepositorios = 0
    let urlPadrao : String = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page="
    var repositorios : Array<Repositorio>! = []
    

    func buscarRepositorios(sucesso:@escaping SucessoRepositorio,falha:@escaping FalhaCarregar,arrayRepositorios:[Repositorio]) {
        
        self.paginaRepositorios += 1
        let url = urlPadrao + String(paginaRepositorios)
        
        Alamofire.request(url).validate().responseJSON { (resposta) in
            
            switch resposta.result {
            case .success:
                
                if let json = resposta.result.value {
                    
                    let dictRepositorios : Dictionary = json as! Dictionary<String, Any>
                    let arrayRepo = dictRepositorios[ConstJson.ITEMS] as? [[String:Any]];
                    for repoAux in arrayRepo! {
                        if let repo :Repositorio = Repositorio.parse(dict: repoAux) {
                            self.repositorios.append(repo)
                        }
                        
                    }
                    sucesso(self.repositorios)
                }
                
            case .failure:
                falha()
            }
            
        }
        
    }
    
    func buscarPulls(nomeRepo:String, nomeUser:String, sucesso:@escaping SucessoPull, falha:@escaping FalhaCarregar) {
        
        let url = "https://api.github.com/repos/\(nomeUser)/\(nomeRepo)/pulls"
        
        Alamofire.request(url).validate().responseJSON { (respostaJson) in
            
            switch respostaJson.result {
                case .success:
                    if let json = respostaJson.result.value {
                        var pulls : Array<Pull> = []
                        let arrayPulls = json as! Array<Dictionary<String,Any>>
                        guard arrayPulls.count > 0 else {
                            falha()
                            return
                        }
                        for pullAux in arrayPulls {
                            print("pull: \(pullAux["user"])")
                            if let pull : Pull = Pull.parse(dict: pullAux) {
                                pulls.append(pull)
                            }
                            
                        }
                        sucesso(pulls)
                    }
                
                case .failure:
                   falha()
            }
            
        }
        
    }
    
    class func downloadFotoUsuario(url:String, sucesso:@escaping SucessoImage) {
        
        Alamofire.request(url).validate().responseData { (responseData) in
            switch responseData.result {
            case .success:
                if let data = responseData.result.value {
                    sucesso(data)
                }
            case .failure(let erro):
                print("Error: \(erro)")
            }
            
        }
    }
}
