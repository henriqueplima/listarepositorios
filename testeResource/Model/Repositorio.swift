//
//  Repositorio.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 10/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit

class Repositorio: NSObject {

    var nome : String?
    var descricao : String?
    var autor : String?
    var estrelas : Int?
    var forks : Int?
    var foto : String?
    
    init(nome:String, descricao:String, autor:String,estrelas:Int,forks:Int, foto:String) {
        
        self.nome = nome
        self.descricao = descricao
        self.autor = autor
        self.estrelas = estrelas
        self.forks = forks
        self.foto = foto
    }
    
    class func parse(dict : Dictionary<String, Any>) -> Repositorio {
        
        let nome : String = dict[ConstJson.REPO_NOME] as! String
        let descricao : String = dict[ConstJson.REPO_DESCRICAO] as! String
        let dictUser : Dictionary<String, Any> = dict[ConstJson.USER] as! Dictionary<String, Any>
        let autor : String = dictUser[ConstJson.AUTOR] as! String
        let foto : String = dictUser[ConstJson.FOTO] as! String
        let estrelas : Int = dict[ConstJson.ESTRELAS] as! Int
        let forks : Int = dict[ConstJson.FORKS] as! Int
        
        
        return Repositorio.init(nome: nome, descricao: descricao , autor: autor, estrelas: estrelas, forks: forks, foto:foto)
        
        
    }
    
    
}
