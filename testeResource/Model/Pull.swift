//
//  Pull.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 11/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit

class Pull: NSObject {
    
    var nomeAutor, fotoAutor, titulo, data, body, urlPagina : String?

    init(title:String, fotoAutor:String, data:String,body:String, autor:String, url:String) {
        
        self.nomeAutor = autor
        self.body = body
        self.data = data
        self.fotoAutor = fotoAutor
        self.titulo = title
        self.urlPagina = url
    }
    
    
    class func parse(dict : Dictionary<String, Any>) -> Pull {
        var data : String = dict[ConstJson.PULL_DATE] as! String
        data = Util.transformaData(data)
        let titulo : String = dict[ConstJson.PULL_TITLE] as! String
        let dictUser : Dictionary<String,Any> = dict[ConstJson.PULL_USER] as! Dictionary<String,Any>
        let autor : String = dictUser[ConstJson.AUTOR] as! String
        let foto : String =  dictUser[ConstJson.FOTO] as! String
        let body : String = dict[ConstJson.PULL_BODY] as! String
        let url = dict[ConstJson.PULL_ULR] as! String
        return Pull.init(title: titulo, fotoAutor: foto, data: data, body: body, autor: autor, url:url)
        
    }

    
}
