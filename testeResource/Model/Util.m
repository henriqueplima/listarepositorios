//
//  Util.m
//  testeResource
//
//  Created by Henrique Pereira de Lima on 12/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

#import "Util.h"

@implementation Util

+ (NSString *)transformaData:(NSString *)dataServidor {
    
    NSString *formatServer = @"yyyy-MM-dd'T'HH:mm:ssZ";
    NSString *formatApp = @"dd/MM/yyyy";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = formatServer;
    NSDate *date = [dateFormatter dateFromString:dataServidor];
    
    dateFormatter.dateFormat = formatApp;
    NSString *dataString = [dateFormatter stringFromDate:date];
    return dataString;
}

+ (void)arredondarImageView:(UIImageView *)imageVIew {
    
    imageVIew.layer.cornerRadius = 10;
    imageVIew.layer.masksToBounds = YES;
    
}

@end
