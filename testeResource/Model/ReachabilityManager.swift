//
//  ReachabilityManager.swift
//  testeResource
//
//  Created by Henrique Pereira de Lima on 12/03/2018.
//  Copyright © 2018 Henrique Pereira de Lima. All rights reserved.
//

import UIKit
import ReachabilitySwift

class ReachabilityManager: NSObject {

    static let shared = ReachabilityManager()
    
    @objc func reachabilityChanged(notification: Notification) {
        
        let reachability = notification.object as? Reachability
        
        switch reachability?.currentReachabilityStatus {
        case .notReachable?:
            print("sem internet")
        case .reachableViaWiFi?:
            print("com wifi")
        case .reachableViaWWAN?:
            print("com rede")
        case .none:
            print("erro")
        }
        
    }
    
    func startMonitoring() {
        let reach = Reachability()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: ReachabilityChangedNotification,
                                               object: reach)
        do{
            try reach?.startNotifier()
        } catch {
            print("erro rede")
        }
    }
    
    class func estaConectado() -> Bool {
        
        let reach = Reachability()
        switch reach?.currentReachabilityStatus {
        case .reachableViaWiFi?, .reachableViaWWAN?:
            return true
        case .notReachable?,.none:
            return false
        }
    }
    
}
